<?php
require_once 'DBConect.php';

$DBCon = DBConect::getInstance();

$query = "SELECT * FROM hobbies";

$res = $DBCon->execSQL($query);

$salida = "";

while($result = $DBCon->singleRow($res,0)){
    if($salida != ""){ $salida .= ", "; }
    $salida .= '{"Nombre":"' . $result['nombre'] . '",';
    $salida .= '"Icon":"' . $result['fa_icon'] . '"}';
}
$salida = '{"Resultados":['.$salida.']}';

echo($salida);
?>
