<?php
/* Clase para gestionar la conexion a la BBDD  for AngulaJS*/

class DBConect{
    private $DBD = array(
        'user' => 'root',
        'pass' => 'pw9811',
        'host' => 'localhost',
        'name' => 'curriculum'

    );
    private $conn;
    private $result;
    private $cadRes;

    static $_instance;


    /* Constructor */
    private function __construct(){
        $this->conectar();
    }

    /* No aceptamos clones (Singleton) */
    private function __clone(){ }

    /* Para instanciar el objeto desde fuera de la clase */
    public static function getInstance(){
        if(!(self::$_instance instanceof self)){
            self::$_instance=new self();
        }
        return self::$_instance;
    }
    
    /* Realiza la conexion */
    private function conectar(){
        $this->conn=mysql_connect(
            $this->DBD['host'], 
            $this->DBD['user'], 
            $this->DBD['pass']
        );

        mysql_select_db($this->DBD['name'], $this->conn);
        @mysql_query("SET NAMES 'utf8'");
    }

    /* Metodo para ejecutar una sentencia */
    public function execSQL($query){
        $this->result=mysql_query($query, $this->conn);

        return $this->result;
    }

    /* Metodo para obtener una fila de resultados de la sentencia de sql (solo una fila)*/
    public function singleRow($result,$row){
        if($row==0){
            $this->cadRes=mysql_fetch_assoc($result);
        }else{
            mysql_data_seek($result,$row);
            $this->cadRes=mysql_fetch_assoc($result);
        }

        return $this->cadRes;
    }

    /* El ultimo ID insertado */
    public function lastID(){
        return mysql_insert_id($this->conn);
    }
}

?>

