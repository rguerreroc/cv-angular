<?php
require_once 'DBConect.php';

$DBCon = DBConect::getInstance();

$query = "SELECT * FROM aboutme";

$res = $DBCon->execSQL($query);

$salida = "";

while($result = $DBCon->singleRow($res,0)){
    if($salida != ""){ $salida .= ", "; }
    $salida .= '{"Nombre":"' . $result['nombre'] . " " . $result['apellido1'] . '",';

    $fecha = explode("-",$result['nacimiento']);
    $nac = $fecha[2]."/".$fecha[1]."/".$fecha[0];

    $salida .= '"Nacimiento":"' . $nac . '",';
    $salida .= '"Ocupacion":"' . $result['ocupacion'] . '",';
    $salida .= '"Imagen":"' . $result['imagen'] . '",';
    $salida .= '"ImagenAlt":"' . base64_encode($result['image_alt']) . '",';
    $salida .= '"Email":"' . $result['email'] . '",';
    $salida .= '"Telefono":"' . $result['telefono'] . '",';
    $salida .= '"Movil":"' . $result['movil'] . '",';
    $salida .= '"WebSite":"' . $result['website'] . '",';
    $salida .= '"Direccion1":"' . $result['direccion1'] . '",';
    $salida .= '"Direccion2":"' . $result['numero'] . ", " . $result['piso'] . " - " . $result['puerta'] . '",';
    $salida .= '"cPostal":"' . $result['cPostal'] . " " . $result['ciudad'] . '",';
    $salida .= '"Pais":"' . $result['pais'] . '"}';
}
$salida = '{"Resultados":['.$salida.']}';

echo($salida);
?>
