<?php
require_once 'DBConect.php';

$DBCon = DBConect::getInstance();

$query = "SELECT * FROM laboral WHERE `columna`= 'izq' ORDER BY `id` DESC";

$res = $DBCon->execSQL($query);

$salida = "";

while($result = $DBCon->singleRow($res,0)){
    if($salida != ""){ $salida .= ", "; }
    $salida .= '{"Titulo":"' . $result['titulo'] . '",';
    $salida .= '"Lugar":"' . $result['lugar'] . '",';
    $salida .= '"FechaIni":"' . $result['fecha_ini'] . '",';
    $salida .= '"FechaFin":"' . $result['fecha_fin'] . '",';
    $salida .= '"Descripcion":"' . $result['fa_icon'] . '"}';
}
$salida = '{"Resultados":['.$salida.']}';

echo($salida);
?>
