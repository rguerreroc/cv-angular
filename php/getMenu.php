<?php
require_once 'DBConect.php';

$DBCon = DBConect::getInstance();

$query = "SELECT * FROM menu";

$res = $DBCon->execSQL($query);

$salida = "";

while($result = $DBCon->singleRow($res,0)){
    if($salida != ""){ $salida .= ", "; }
    $salida .= '{"Id":"' . $result['id'] . '",';
    $salida .= '"Class":"' . $result['class'] . '",';
    $salida .= '"Path":"' . $result['path'] . '",';
    $salida .= '"Descripcion":"' . $result['desc'] . '"}';
}
$salida = '{"Resultados":['.$salida.']}';

echo($salida);
?>
