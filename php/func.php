<?php 
include('conf.php');

function conDB(){
    global $dbDatos;

    $con = mysql_connect(
        $dbDatos['dbHost'],
        $dbDatos['dbUser'],
        $dbDatos['dbPass']
    );

    if(!$con){
        die('No pudo conectarse al servidor de BDD: '.mysql_error());
    }else{
        mysql_select_db($dbDatos['dbName'], $con);
        return $con;
    }
}

function recDatos($tipo){
    $con = conDB();

    $query = "SELECT * FROM $tipo";

    $result = mysql_query($query, $con);

    return $result;
}
function recDatosOrd($tipo, $column){
    $con = conDB();

    $query = "SELECT * FROM $tipo ORDER BY `id` DESC";

    $result = mysql_query($query, $con);

    return $result;
}
function showDatos($tipo){
    $lineas = recDatos($tipo);

    $result = "<ul class='$tipo'>";
    while($rows = mysql_fetch_assoc($lineas)){
        switch ($tipo){
        case "social":
            $result .= "<li><a class='".$rows['red']."' href='".$rows['url']."'></a></li>";
            break;
        case "hobbies":
            $result .= "<li><p><i class='".$rows['fa_icon']."'></i><br><span>".$rows['nombre']."</span></p></li>";
            break;
        }
    }
    $result .= "</ul>";

    return $result;
}

function showDatLeft($tipo, $column){
    $lineas = recDatosOrd($tipo, $column);
    $result = "";
    while($rows = mysql_fetch_assoc($lineas)){
        if($rows['fecha_fin'] == '0000-00-00'){
            $fechafin = "Actualidad";
        }else{
            $fechafin = changeOrder($rows['fecha_fin']);
        }
        if($rows['columna']=="izq"){
            $fechaini = changeOrder($rows['fecha_ini']);
            $result .= "<div class='timeline-post'>";
            $result .= "<div class='timeline-post-content-holder'>";
            $result .= "<div class='timeline-post-icon'></div>";
            $result .= "<div class='timeline-post-title'>";
            $result .= "<h4>".$rows['titulo']."</h4>";
            $result .= "</div>";
            $result .= "<div class='timeline-post-subtitle'>";
            $result .= "<p><span>".$rows['lugar']."</span><span class='timeline-duration'>".$fechaini." - ".$fechafin."</span></p>";
            $result .= "</div>";
            $result .= "<div class='timeline-post-content'>";
            $result .= $rows['descripcion'];
            $result .= "</div>";
            $result .= "</div> <!-- /.timeline-post-content-holder end -->";
            $result .= "</div> <!-- /.timeline-post end -->";
        }
    }

    return $result;
}

function showDatRigth($tipo, $column){
    $lineas = recDatosOrd($tipo, $column);
    $result = "";
    while($rows = mysql_fetch_assoc($lineas)){
        if($rows['fecha_fin'] == '0000-00-00'){
            $fechafin = "Actualidad";
        }else{
            $fechafin = changeOrder($rows['fecha_fin']);
        }
        if($rows['columna']=="der"){
            $fechaini = changeOrder($rows['fecha_ini']);
            $result .= "<div class='timeline-post'>";
            $result .= "<div class='timeline-post-content-holder'>";
            $result .= "<div class='timeline-post-icon'></div>";
            $result .= "<div class='timeline-post-title'>";
            $result .= "<h4>".$rows['titulo']."</h4>";
            $result .= "</div>";
            $result .= "<div class='timeline-post-subtitle'>";
            $result .= "<p><span>".$rows['lugar']."</span><span class='timeline-duration'>".$fechaini." - ".$fechafin."</span></p>";
            $result .= "</div>";
            $result .= "<div class='timeline-post-content'>";
            $result .= $rows['descripcion'];
            $result .= "</div>";
            $result .= "</div> <!-- /.timeline-post-content-holder end -->";
            $result .= "</div> <!-- /.timeline-post end -->";
        }
    }

    return $result;
}


function showHab($tipo){
    $lineas = recDatos($tipo);
    $result = "";
    while($rows = mysql_fetch_assoc($lineas)){
        $result .= "<div class='col-xs-12 col-sm-6 col-md-3 chart-padding'>";
        $result .= "<div class='chart' data-percent='".$rows['porcentaje']."'><p>".$rows['descripcion']."</p></div>";
        $result .= "<div class='skills-content'>";
        $result .= "<h3>".$rows['nombre']."</h3>";
        $result .= "</div><!-- end .skils-content -->";
        $result .= "</div><!-- end .col-xs-12 .col-sm-6 .col-md-3 .chart-padding -->";
    }

    return $result;
}

function changeOrder($fecha){
    $fechas = explode("-", $fecha);

    $fechaNueva = $fechas[1] ."/". $fechas[0];

    return $fechaNueva;
}

function aboutMe(){
    $misDatos = recDatos("aboutme");
    $lineas = mysql_fetch_assoc($misDatos);

    $fecha = explode("-",$lineas['nacimiento']);
    $nac = $fecha[2]."/".$fecha[1]."/".$fecha[0];

    $result = "<div class='info-details'>";
    $result .= "<p>: ".$nac."</p>";
    $result .= "<p>: ".$lineas['telefono']."</p>";
    $result .= "<p>: ".$lineas['movil']."</p>";
    $result .= "<p>: ".$lineas['email']."</p>";
    $result .= "<p>: ".$lineas['website']."</p>";
    $result .= "<p>: ".$lineas['direccion1']."</p>";
    $result .= "<p>: ".$lineas['numero'].", ".$lineas['piso']." ".$lineas['puerta']."</p>";
    $result .= "<p>: 0".$lineas['cPostal']."</p>";
    $result .= "<p>: ".$lineas['pais']."</p>";
    $result .= "</div><!-- end .info-details -->";

    return $result;
}

function getOneData($tabla, $columna){
    $con = conDB();

    $query = "SELECT $columna FROM $tabla";

    $result  = mysql_query($query, $con);

    $data = mysql_fetch_assoc($result);
    return $data[$columna];
}


?>
