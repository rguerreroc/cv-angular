'use strinct';
var ang = angular.module('ang', [ ]);

ang.controller('menu', function($scope, $http){
    $http.get("php/getMenu.php")
    .success(function(data){
        $scope.menu = data.Resultados;
    });
});

ang.controller('aboutme', function($scope, $http){
    $http.get("php/getAboutMe.php")
    .success(function(data){
        $scope.aboutme = data.Resultados;
    });
});

ang.controller('social', function($scope, $http){
    $http.get("php/getSocial.php")
    .success(function(data){
        $scope.social = data.Resultados;
    })
});

ang.controller('hobbies', function($scope, $http){
    $http.get("php/getHobbies.php")
    .success(function(data){
        $scope.hobbies = data.Resultados;
    })
});

ang.controller('xpLeft', function($scope, $http){
    $http.get("php/getXPLeft.php")
    .success(function(data){
        $scope.hobbies = data.Resultados;
    })
});

ang.controller('xpRight', function($scope, $http){
    $http.get("php/getXPRight.php")
    .success(function(data){
        $scope.hobbies = data.Resultados;
    })
});
