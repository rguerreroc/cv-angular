$(function(){
    $("#mailSend").click(function(){
        $.ajax({
            type: "POST",
            url: "../php/mailer.php",
            data: {
                action: 'sendMail',
                sender: $("#sender").val(),
                email: $("#mail").val(),
                msg: $("#msg").val()
            }
        }).done(function(data){
            $("#formDiv").html(data);
        });
    })
});
